export default class Tiles {
    static get(file, tile) {
        return game.tileManager.listFile[file].listItem[this[tile.namespace][tile.key]];
    }
}

Tiles.MAP = {};
Tiles.MAP.GRASS = 0;
Tiles.MAP.FLOWERS = 1;
Tiles.MAP.TREE = 2;
Tiles.MAP.UPTREE = 3;
Tiles.MAP.BUSH = 4;
Tiles.MAP.FLOWERS2 = 5;

Tiles.CHAR = {};
Tiles.CHAR.IDLE0 = 0;
Tiles.CHAR.IDLE1 = 1;
Tiles.CHAR.IDLE2 = 2;
Tiles.CHAR.IDLE3 = 3;
Tiles.CHAR.BOTTOM0 = 4;
Tiles.CHAR.BOTTOM1 = 5;
Tiles.CHAR.BOTTOM2 = 6;
Tiles.CHAR.BOTTOM3 = 7;
Tiles.CHAR.BOTTOM4 = 8;
Tiles.CHAR.TOP0 = 9;
Tiles.CHAR.TOP1 = 10;
Tiles.CHAR.TOP2 = 11;
Tiles.CHAR.TOP3 = 12;
Tiles.CHAR.TOP4 = 13;
Tiles.CHAR.RIGHT0 = 14;
Tiles.CHAR.RIGHT1 = 15;
Tiles.CHAR.RIGHT2 = 16;
Tiles.CHAR.RIGHT3 = 17;
Tiles.CHAR.RIGHT4 = 18;
Tiles.CHAR.RIGHT5 = 19;
Tiles.CHAR.LEFT0 = 20;
Tiles.CHAR.LEFT1 = 21;
Tiles.CHAR.LEFT2 = 22;
Tiles.CHAR.LEFT3 = 23;
Tiles.CHAR.LEFT4 = 24;
Tiles.CHAR.LEFT5 = 25;
Tiles.CHAR.ATTACKBOTTOM0 = 26;
Tiles.CHAR.ATTACKBOTTOM1 = 27;
Tiles.CHAR.ATTACKBOTTOM2 = 28;
Tiles.CHAR.ATTACKTOP0 = 29;
Tiles.CHAR.ATTACKTOP1 = 30;
Tiles.CHAR.ATTACKTOP2 = 31;
Tiles.CHAR.ATTACKRIGHT0 = 32;
Tiles.CHAR.ATTACKRIGHT1 = 33;
Tiles.CHAR.ATTACKRIGHT2 = 34;
Tiles.CHAR.ATTACKRIGHT0 = 35;
Tiles.CHAR.ATTACKRIGHT1 = 36;
Tiles.CHAR.ATTACKRIGHT2 = 37;
