

export default class Square {
    constructor(x, y) {
        this.x = x;
        this.y = y;

        switch (Math.floor(Math.random() * 10)) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                this.tile = {
                    namespace: "MAP",
                    key: "GRASS"
                };
                break;


            case 6:
                this.tile = {
                    namespace: "MAP",
                    key: "FLOWERS"
                };
                break;

            case 7:
                this.tile = {
                    namespace: "MAP",
                    key: "TREE"
                };
                break;

            case 8:
                this.tile = {
                    namespace: "MAP",
                    key: "BUSH"
                };
                break;


            case 9:
                this.tile = {
                    namespace: "MAP",
                    key: "FLOWERS2"
                };
                break;
        }
    }

    display() {
        if (this.tile.key == "BUSH") {
            game.canvas.draw("tiles2", {namespace: "MAP",key: "GRASS"}, {x: this.x,y: this.y});
        }

        game.canvas.draw("tiles2", this.tile, {x: this.x,y: this.y});

        if (this.tile.key == "TREE") {
            game.canvas.draw("tiles2", {namespace: "MAP",key: "UPTREE"}, {x: this.x,y: this.y - 1});
        }
    }
}