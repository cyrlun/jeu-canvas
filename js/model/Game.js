
import Map from "/js/model/Map.js";
import Canvas from "/js/model/Canvas.js";
import TileManager from "/js/model/TileManager.js";
import Character from "/js/model/Character.js";

export default class Game {
    constructor(){
        this.canvas = new Canvas(document.querySelector("#static-canvas"));
    }
    async init(){
        this.tileManager = new TileManager();
        await this.tileManager.loadFile("map","tiles2", 6, 1);
        await this.tileManager.loadFile("character","char1", 4, 4);
        await this.tileManager.loadFile("character","char2", 4, 4);
        await this.tileManager.loadFile("character","char3", 4, 4);
        await this.tileManager.loadFile("character","knight iso char", 8, 5);

        this.map = new Map();
        
        await this.map.loadMap("map1");
        
        this.canvas.setStep(this.map.size);
        
        this.map.display();

        this.character = new Array();
       // let char1 = new Character("Cyrlun","Monk",5,5,"BOTTOM")
       // let char2 = new Character("Opalkia","Man",5,4,"LEFT")
        let char4 = new Character("Thorkann","Rogue",5,3,"IDLE")

       // char1.display();
       // char1.animate();

       // char2.display();
       // char2.animate();
        char4.display();
        char4.animate();
    }
    display(element){
        switch (element.constructor.name) {
            case "Square":
                this.canvas.display(element);
        }
    }
}